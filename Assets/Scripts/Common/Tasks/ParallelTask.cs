﻿using System.Collections.Generic;
using Common.Enums;

namespace Common.Tasks
{
    public class ParallelTask : BaseTask
    {
        private int _taskCompleteCount;
        private int _taskCount;
        private List<BaseTask> _tasks;

        private void Awake()
        {
            TaskDelegate completeDelegate = OnChildTaskComplete;
            TaskDelegate guardDelegate = OnChildTaskGuard;
            _taskCount = transform.childCount;
            _taskCompleteCount = 0;
            _tasks = new List<BaseTask>();
            for (var i = 0; i < _taskCount; i++)
            {
                var task = transform.GetChild(i).GetComponent<BaseTask>();
                task.onTaskComplete = completeDelegate;
                task.onTaskGuard = guardDelegate;
                _tasks.Add(task);
            }
        }

        protected override void DeActivate()
        {
            _taskCompleteCount = 0;
            for (var i = 0; i < _taskCount; i++) _tasks[i].forceMode = false;

            base.DeActivate();
        }

        private void OnChildTaskComplete(BaseTask task)
        {
            _taskCompleteCount++;
            if (_taskCompleteCount == _taskCount) Complete();
        }

        private void OnChildTaskGuard(BaseTask task)
        {
            _taskCompleteCount++;
            if (_taskCompleteCount == _taskCount) Complete();
        }

        protected override void OnForceExecute()
        {
            OnExecute();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _taskCompleteCount = 0;
            for (var i = 0; i < _taskCount; i++) _tasks[i].Execute();
        }

        public override void ForceModeOn()
        {
            base.ForceModeOn();
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOn();
        }

        protected internal override void OnTerminate()
        {
            for (var i = 0; i < _taskCount; i++) _tasks[i].Terminate();
            base.OnTerminate();
        }
    }
}