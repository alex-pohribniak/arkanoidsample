﻿using System.Collections.Generic;
using Common.Enums;

namespace Common.Tasks
{
    public class SequenceTask : BaseTask
    {
        private BaseTask _currentTask;
        private int _currentTaskIndex;
        private int _taskCount;
        private List<BaseTask> _tasks;

        private void Awake()
        {
            TaskDelegate completeDelegate = OnChildTaskComplete;
            TaskDelegate guardDelegate = OnChildTaskGuard;
            _taskCount = transform.childCount;
            _tasks = new List<BaseTask>();
            for (var i = 0; i < _taskCount; i++)
            {
                var task = transform.GetChild(i).GetComponent<BaseTask>();
                task.onTaskComplete = completeDelegate;
                task.onTaskGuard = guardDelegate;
                _tasks.Add(task);
            }
        }

        protected override void DeActivate()
        {
            _currentTask = null;
            _currentTaskIndex = -1;
            for (var i = 0; i < _taskCount; i++) _tasks[i].forceMode = false;

            base.DeActivate();
        }

        private void OnChildTaskComplete(BaseTask task)
        {
            if (_currentTask == task) NextTask();
        }

        private void OnChildTaskGuard(BaseTask task)
        {
            if (_currentTask == task) NextTask();
        }

        protected override void OnForceExecute()
        {
            OnExecute();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            _currentTask = null;
            _currentTaskIndex = -1;
            NextTask();
        }

        private void NextTask()
        {
            _currentTaskIndex++;
            if (_currentTaskIndex < _taskCount)
            {
                _currentTask = _tasks[_currentTaskIndex];
                _currentTask.Execute();
            }
            else
            {
                Complete();
            }
        }

        public override void ForceModeOn()
        {
            base.ForceModeOn();
            for (var i = 0; i < _taskCount; i++) _tasks[i].ForceModeOn();
        }

        protected internal override void OnTerminate()
        {
            if (_currentTask != null) _currentTask.OnTerminate();
            base.OnTerminate();
        }
    }
}