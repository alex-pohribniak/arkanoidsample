﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ArkanoidSample.Model
{
    [Serializable]
    public class ArkanoidSampleData
    {
        public bool win;
        public int life;
        public bool ballExist;
        public bool shootBall;

        public List<GameObject> vulnerableBlocks;
    }
}