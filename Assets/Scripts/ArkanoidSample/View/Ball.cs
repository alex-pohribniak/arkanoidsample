﻿using ArkanoidSample.Config;
using UnityEngine;

namespace ArkanoidSample.View
{
    public class Ball : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.name != "Platform") return;
            var xValue = HitFactor(transform.position, other.transform.position, other.collider.bounds.size.x);
            var direction = new Vector2(xValue, 1).normalized;
            GetComponent<Rigidbody2D>().velocity = direction * BallConfig.Speed;
        }

        private static float HitFactor(Vector2 ballPosition, Vector2 platformPosition, float platformWidth)
        {
            return (ballPosition.x - platformPosition.x) / platformWidth;
        }
    }
}