﻿using ArkanoidSample.Enums;
using Common.Singleton;
using UnityEngine;

namespace ArkanoidSample.View
{
    public class SimpleBlock : MonoBehaviour
    {
        public ParticleSystem prefabDestroy;
        private void OnCollisionEnter2D(Collision2D other)
        {
            var destroy = Instantiate(prefabDestroy, gameObject.transform.parent);
            destroy.transform.position = gameObject.transform.position;
            destroy.Play();
            Destroy(destroy, 1f);
            Destroy(gameObject);
            Observer.Emit(ArkanoidSampleEvent.SubtractBlock);
        }
    }
}