﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ArkanoidSample.View
{
    public class SpritesLink : MonoBehaviour
    {
        public List<Sprite> sprites;

        private void Awake()
        {
            sprites = Resources.LoadAll<Sprite>("ArkanoidSample/Sprites/Blocks").ToList();
        }
    }
}