﻿using ArkanoidSample.Config;
using ArkanoidSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ArkanoidSample.View
{
    public class View : MonoBehaviour, IPointerDownHandler
    {
        public GameObject ball;
        public Text counter;
        public Image lifeImage;
        public Image movePlatformHitArea;
        public GameObject platform;
        public Image shootBallHitArea;
        public Camera uiCamera;

        private readonly SimpleDelegate _onCreateBall;
        private readonly SimpleDelegate _onMovePlatform;
        private readonly SimpleDelegate _onShootBall;
        private readonly SimpleDelegate _onStopPlatform;
        private readonly SimpleDelegate _onUpdateView;
        private GameObject _ballClone;

        private bool _move;

        private Vector3 _targetPosition;


        public View()
        {
            _onStopPlatform = OnStopPlatform;
            _onUpdateView = OnUpdateView;
            _onCreateBall = OnCreateBall;
            _onShootBall = OnShootBall;
            _onMovePlatform = OnMovePlatform;
        }

        private void Awake()
        {
            ball.SetActive(false);
            movePlatformHitArea.raycastTarget = false;
            shootBallHitArea.raycastTarget = false;
            Observer.AddListener(ArkanoidSampleEvent.MovePlatform, _onMovePlatform);
            Observer.AddListener(ArkanoidSampleEvent.ShootBall, _onShootBall);
            Observer.AddListener(ArkanoidSampleEvent.CreateBall, _onCreateBall);
            Observer.AddListener(ArkanoidSampleEvent.UpdateView, _onUpdateView);
            Observer.AddListener(ArkanoidSampleEvent.StopPlatform, _onStopPlatform);
        }

        private void FixedUpdate()
        {
            if (!_move) return;
            var position = platform.transform.position;
            var direction = (_targetPosition - position).normalized;
            platform.GetComponent<Rigidbody2D>()
                .MovePosition(position + direction * PlatformConfig.Speed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ArkanoidSampleEvent.MovePlatform, _onMovePlatform);
            Observer.RemoveListener(ArkanoidSampleEvent.ShootBall, _onShootBall);
            Observer.RemoveListener(ArkanoidSampleEvent.CreateBall, _onCreateBall);
            Observer.RemoveListener(ArkanoidSampleEvent.UpdateView, _onUpdateView);
            Observer.RemoveListener(ArkanoidSampleEvent.StopPlatform, _onStopPlatform);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _targetPosition = uiCamera.ScreenToWorldPoint(eventData.position);
        }

        private void OnStopPlatform()
        {
            _move = false;
        }

        private void OnUpdateView()
        {
            var life = MainModel.ArkanoidSampleData.life;
            counter.text = life.ToString();
            if (life == 0) lifeImage.color = Color.black;
        }

        private void OnCreateBall()
        {
            var spawnPosition = new Vector3(platform.GetComponent<BoxCollider2D>().bounds.center.x,
                ball.transform.position.y, 1f);
            _ballClone = Instantiate(ball, spawnPosition, Quaternion.identity, ball.transform.parent);
            _ballClone.SetActive(true);
            SwitchHitArea();
        }

        private void OnShootBall()
        {
            var direction = (_targetPosition - _ballClone.transform.position).normalized;
            _ballClone.GetComponent<Rigidbody2D>().velocity = direction * BallConfig.Speed;
            _ballClone.GetComponent<ParticleSystem>().Play();
            Observer.Emit(ArkanoidSampleEvent.ShootBallComplete);
            SwitchHitArea();
        }

        private void OnMovePlatform()
        {
            if (MainModel.ArkanoidSampleData.shootBall) _move = true;
        }

        private void SwitchHitArea()
        {
            if (shootBallHitArea.raycastTarget == false)
            {
                shootBallHitArea.raycastTarget = true;
                movePlatformHitArea.raycastTarget = false;
            }
            else
            {
                movePlatformHitArea.raycastTarget = true;
                shootBallHitArea.raycastTarget = false;
            }
        }
    }
}