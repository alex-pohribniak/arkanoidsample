﻿using ArkanoidSample.Enums;
using Common.Singleton;
using UnityEngine;

namespace ArkanoidSample.View
{
    public class BallDestroyer : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            Destroy(other.gameObject);
            Observer.Emit(ArkanoidSampleEvent.BallDestroyed);
        }
    }
}