﻿using UnityEngine;

namespace ArkanoidSample.View
{
    public class InvulnerableBlock : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            GetComponent<ParticleSystem>().Play();
        }
    }
}