﻿using ArkanoidSample.Enums;
using Common.Singleton;
using UnityEngine;

namespace ArkanoidSample.View
{
    public class DurableBlock : MonoBehaviour
    {
        public int durability = 3;
        public ParticleSystem prefabDestroy;
        public SpritesLink spritesLink;

        private void OnCollisionEnter2D(Collision2D other)
        {
            GetComponent<ParticleSystem>().Play();
            durability--;
            ChangeSprite();
            if (durability != 0) return;
            var destroy = Instantiate(prefabDestroy, gameObject.transform.parent);
            destroy.transform.position = gameObject.transform.position;
            destroy.Play();
            Destroy(destroy, 1f);
            Destroy(gameObject);
            Observer.Emit(ArkanoidSampleEvent.SubtractBlock);
        }

        private void ChangeSprite()
        {
            switch (durability)
            {
                case 1:
                    gameObject.GetComponent<SpriteRenderer>().sprite = spritesLink.sprites[2];
                    break;
                case 2:
                    gameObject.GetComponent<SpriteRenderer>().sprite = spritesLink.sprites[1];
                    break;
            }
        }
    }
}