﻿using ArkanoidSample.Enums;
using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ArkanoidSample.View
{
    public class ResultView : MonoBehaviour
    {
        public Image result;

        private readonly SimpleDelegate _onShowResult;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ResultView()
        {
            _onShowResult = OnShowResult;
        }

        protected void Awake()
        {
            result.gameObject.SetActive(false);
            Observer.AddListener(ArkanoidSampleEvent.ShowResult, _onShowResult);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ArkanoidSampleEvent.ShowResult, _onShowResult);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowResult()
        {
            result.gameObject.SetActive(true);
            _tweenDuration = 3f;
            _tweenAnimation = DOTween.Sequence();
            result.sprite = DefineResultSprite();
            var tweenFade = result.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() => { Observer.Emit(ArkanoidSampleEvent.ShowResultComplete); });
            _tweenAnimation.Play();
        }

        private static Sprite DefineResultSprite()
        {
            if (MainModel.ArkanoidSampleData.win) return Resources.Load<Sprite>("Common/Sprites/Win");
            return !MainModel.ArkanoidSampleData.win ? Resources.Load<Sprite>("Common/Sprites/GameOver") : default;
        }
    }
}