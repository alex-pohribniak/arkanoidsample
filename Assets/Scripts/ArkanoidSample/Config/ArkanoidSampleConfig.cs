﻿using ArkanoidSample.Model;
using UnityEngine;

namespace ArkanoidSample.Config
{
    public class ArkanoidSampleConfig : MonoBehaviour
    {
        public ArkanoidSampleData arkanoidSampleData;
    }
}