﻿using Common.Tasks;
using UnityEngine.SceneManagement;

namespace ArkanoidSample.Logic
{
    public class EndArkanoidSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            SceneManager.LoadScene("ArkanoidSample");
            Complete();
        }
    }
}