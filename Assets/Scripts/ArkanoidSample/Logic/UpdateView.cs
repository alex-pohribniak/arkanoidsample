﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class UpdateView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ArkanoidSampleEvent.UpdateView);
            Complete();
        }
    }
}