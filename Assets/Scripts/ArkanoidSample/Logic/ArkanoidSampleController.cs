﻿using Common.Tasks;
using UnityEngine;

namespace ArkanoidSample.Logic
{
    public class ArkanoidSampleController : MonoBehaviour
    {
        public BaseTask movePlatform;
        public BaseTask shootBall;

        public void OnClick()
        {
            movePlatform.Execute();
            shootBall.Execute();
        }
    }
}