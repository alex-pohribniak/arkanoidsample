﻿using ArkanoidSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class ShootBall : BaseTask
    {
        private readonly SimpleDelegate _onShootBallComplete;

        public ShootBall()
        {
            _onShootBallComplete = OnShootBallComplete;
        }

        private void OnShootBallComplete()
        {
            MainModel.ArkanoidSampleData.shootBall = true;
            Complete();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ArkanoidSampleEvent.ShootBall);
        }

        protected override bool Guard()
        {
            return !MainModel.ArkanoidSampleData.shootBall;
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(ArkanoidSampleEvent.ShootBallComplete, _onShootBallComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            Observer.RemoveListener(ArkanoidSampleEvent.ShootBallComplete, _onShootBallComplete);
        }
    }
}