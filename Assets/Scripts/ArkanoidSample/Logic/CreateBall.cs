﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class CreateBall : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ArkanoidSampleData.ballExist = true;
            Observer.Emit(ArkanoidSampleEvent.CreateBall);
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.ArkanoidSampleData.life > 0;
        }
    }
}