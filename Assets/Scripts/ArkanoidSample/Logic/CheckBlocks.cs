﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class CheckBlocks : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            if (MainModel.ArkanoidSampleData.vulnerableBlocks.Count == 0)
            {
                MainModel.ArkanoidSampleData.win = true;
                Observer.Emit(ArkanoidSampleEvent.PlayerWon);
            }

            Complete();
        }
    }
}