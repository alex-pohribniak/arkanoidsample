﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class MovePlatform : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ArkanoidSampleEvent.MovePlatform);
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.ArkanoidSampleData.ballExist;
        }
    }
}