﻿using ArkanoidSample.Config;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class InitArkanoidSample : BaseTask
    {
        public ArkanoidSampleConfig arkanoidSampleConfig;

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ArkanoidSampleData = arkanoidSampleConfig.arkanoidSampleData;
            Complete();
        }
    }
}