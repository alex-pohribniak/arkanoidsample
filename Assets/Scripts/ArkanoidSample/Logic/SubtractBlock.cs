﻿using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class SubtractBlock : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var blocks = MainModel.ArkanoidSampleData.vulnerableBlocks;
            blocks.RemoveAt(blocks.Count - 1);
            Complete();
        }
    }
}