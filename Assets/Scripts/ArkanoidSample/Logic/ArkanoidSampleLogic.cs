﻿using ArkanoidSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using UnityEngine;

namespace ArkanoidSample.Logic
{
    public class ArkanoidSampleLogic : MonoBehaviour
    {
        private readonly SimpleDelegate _onBallDestroyed;
        private readonly SimpleDelegate _onPlayerLost;
        private readonly SimpleDelegate _onPlayerWon;
        private readonly SimpleDelegate _onSubtractBlock;

        public BaseTask endArkanoidSample;
        public BaseTask renewArkanoidSample;
        public BaseTask startTask;
        public BaseTask subtractBlock;

        public ArkanoidSampleLogic()
        {
            _onSubtractBlock = OnSubtractBlock;
            _onBallDestroyed = OnBallDestroyed;
            _onPlayerLost = OnPlayerLost;
            _onPlayerWon = OnPlayerWon;
        }

        private void OnSubtractBlock()
        {
            subtractBlock.Execute();
        }

        private void OnBallDestroyed()
        {
            MainModel.ArkanoidSampleData.shootBall = false;
            MainModel.ArkanoidSampleData.ballExist = false;
            renewArkanoidSample.Execute();
        }

        private void OnPlayerLost()
        {
            endArkanoidSample.Execute();
        }

        private void OnPlayerWon()
        {
            endArkanoidSample.Execute();
        }

        private void Start()
        {
            startTask.Execute();
        }

        private void Awake()
        {
            Observer.AddListener(ArkanoidSampleEvent.PlayerWon, _onPlayerWon);
            Observer.AddListener(ArkanoidSampleEvent.PlayerLost, _onPlayerLost);
            Observer.AddListener(ArkanoidSampleEvent.BallDestroyed, _onBallDestroyed);
            Observer.AddListener(ArkanoidSampleEvent.SubtractBlock, _onSubtractBlock);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ArkanoidSampleEvent.PlayerWon, _onPlayerWon);
            Observer.RemoveListener(ArkanoidSampleEvent.PlayerLost, _onPlayerLost);
            Observer.RemoveListener(ArkanoidSampleEvent.BallDestroyed, _onBallDestroyed);
            Observer.RemoveListener(ArkanoidSampleEvent.SubtractBlock, _onSubtractBlock);
        }
    }
}