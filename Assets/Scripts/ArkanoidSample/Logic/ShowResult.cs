﻿using ArkanoidSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class ShowResult : BaseTask
    {
        private readonly SimpleDelegate _onShowResultComplete;

        public ShowResult()
        {
            _onShowResultComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ArkanoidSampleEvent.ShowResult);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(ArkanoidSampleEvent.ShowResultComplete, _onShowResultComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ArkanoidSampleEvent.ShowResultComplete, _onShowResultComplete);
        }
    }
}