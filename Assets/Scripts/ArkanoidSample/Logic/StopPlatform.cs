﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class StopPlatform : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ArkanoidSampleEvent.StopPlatform);
            Complete();
        }
    }
}