﻿using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class ReduceLife : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ArkanoidSampleData.life--;
            Complete();
        }
    }
}