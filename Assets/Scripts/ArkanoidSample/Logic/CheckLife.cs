﻿using ArkanoidSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace ArkanoidSample.Logic
{
    public class CheckLife : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            if (MainModel.ArkanoidSampleData.life == 0)
            {
                MainModel.ArkanoidSampleData.win = false;
                Observer.Emit(ArkanoidSampleEvent.PlayerLost);
            }
            Complete();
        }
    }
}