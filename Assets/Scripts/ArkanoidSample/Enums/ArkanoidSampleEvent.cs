﻿namespace ArkanoidSample.Enums
{
    public static class ArkanoidSampleEvent
    {
        public const string PlayerWon = "PlayerWon";
        public const string PlayerLost = "PlayerLost";
        public const string MovePlatform = "MovePlatform";
        public const string ShootBall = "ShootBall";
        public const string ShootBallComplete = "ShootBallComplete";
        public const string BallDestroyed = "BallDestroyed";
        public const string StopPlatform = "StopPlatform";
        public const string UpdateView = "UpdateView";
        public const string CreateBall = "CreateBall";
        public const string SubtractBlock = "SubtractBlock";
        public const string ShowResult = "ShowResult";
        public const string ShowResultComplete = "ShowResultComplete";
    }
}